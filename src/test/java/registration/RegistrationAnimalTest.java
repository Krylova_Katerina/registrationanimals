package registration;

import org.junit.Assert;
import org.junit.Test;
import registration.data.Animal;
import registration.file.TextFileWorkImpl;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RegistrationAnimalTest {

    @Test
    public void validateAnimalByRulesEqualSimple() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> rule = new HashMap<>();
        rule.put("ТИП", "ТРАВОЯДНОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);

        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, rule, true));

    }

    @Test
    public void validateAnimalByRulesOrEqual() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> rule = new HashMap<>();
        rule.put("ТИП", "ТРАВОЯДНОЕ ИЛИ ВСЕЯДНОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, rule, true));

    }

    @Test
    public void validateAnimalByRulesNotEqualSimple() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> rule = new HashMap<>();
        rule.put("ТИП", "ТРАВОЯДНОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Assert.assertFalse(registrationAnimals.validateAnimalByRules(animal, rule, false));
    }

    @Test
    public void validateAnimalByRulesOrNotEqual() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> rule = new HashMap<>();
        rule.put("ТИП", "ТРАВОЯДНОЕ ИЛИ ВСЕЯДНОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, rule, false));
    }


    @Test
    public void validateAnimalBySomeRulesEqualSimple() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> rule = new HashMap<>();
        rule.put("ТИП", "ТРАВОЯДНОЕ");
        rule.put("РОСТ", "НЕВЫСОКОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);

        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, rule, true));

    }

    @Test
    public void validateAnimalBySomeRulesEqualAndNotEqualSimple() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> ruleEqual = new HashMap<>();
        ruleEqual.put("ТИП", "ТРАВОЯДНОЕ");

        Map<String, String> ruleNotEqual = new HashMap<>();
        ruleNotEqual.put("РОСТ", "МАЛЕНЬКОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);

        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, ruleEqual, true) &&
                registrationAnimals.validateAnimalByRules(animal, ruleNotEqual, false));
    }

    @Test
    public void validateAnimalBySomeRulesEqualAndNotEqual() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> ruleEqual = new HashMap<>();
        ruleEqual.put("ТИП", "ТРАВОЯДНОЕ");

        Map<String, String> ruleNotEqual = new HashMap<>();
        ruleNotEqual.put("РОСТ", "МАЛЕНЬКОЕ или НЕВЫСОКОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);

        Assert.assertTrue(registrationAnimals.validateAnimalByRules(animal, ruleEqual, true)
                && registrationAnimals.validateAnimalByRules(animal, ruleNotEqual, false));
    }

    @Test
    public void validateAnimalBySomeRulesEqualAndNotEqualFalse() {
        Animal animal = createAnimalForTest("ТЯЖЕЛОЕ", "НЕВЫСОКОЕ", "ТРАВОЯДНОЕ");
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        Map<String, String> ruleEqual = new HashMap<>();
        ruleEqual.put("ТИП", "ТРАВОЯДНОЕ");
        Map<String, String> ruleNotEqual = new HashMap<>();
        ruleNotEqual.put("РОСТ", "МАЛЕНЬКОЕ или НЕВЫСОКОЕ");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);

        Assert.assertFalse(registrationAnimals.validateAnimalByRules(animal, ruleEqual, false)
                && registrationAnimals.validateAnimalByRules(animal, ruleNotEqual, false));
    }

    @Test
    public void registrationAnimalsByRules() {
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Long actualCount = registrationAnimals.registrationAnimalsByRules();
        Long expectedCount = 6L;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void registrationAnimalsByRulesNoAnimals() {
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFileSecond.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Long actualCount = registrationAnimals.registrationAnimalsByRules();
        Long expectedCount = 0L;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void registrationAnimalsByRuleFirst() {
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFirst.txt",
                "./src/test/resources/SignFile.txt");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Long actualCount = registrationAnimals.registrationAnimalsByRules();
        Long expectedCount = 7L;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void registrationAnimalsByRuleSecond() {
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleSecond.txt",
                "./src/test/resources/SignFile.txt");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Long actualCount = registrationAnimals.registrationAnimalsByRules();
        Long expectedCount = 5L;
        assertEquals(expectedCount, actualCount);
    }

    @Test
    public void registrationAnimalsByRuleThird() {
        TextFileWorkImpl textFileWork = createTextFileWorkForTest("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleThird.txt",
                "./src/test/resources/SignFile.txt");
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        Long actualCount = registrationAnimals.registrationAnimalsByRules();
        Long expectedCount = 3L;
        assertEquals(expectedCount, actualCount);
    }


    private Animal createAnimalForTest(String weight, String growth, String type) {
        Animal animal = new Animal();
        Map<String, String> properties = new HashMap<>();
        properties.put("ВЕС", weight);
        properties.put("РОСТ", growth);
        properties.put("ТИП", type);
        animal.setProperties(properties);
        return animal;
    }

    private TextFileWorkImpl createTextFileWorkForTest(String pathAnimal, String pathRules, String pathSigns) {
        return new TextFileWorkImpl(pathAnimal, pathRules, pathSigns);
    }
}
