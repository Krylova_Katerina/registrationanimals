package registration.data;


import org.junit.Assert;
import org.junit.Test;
import registration.exception.ProhibitedActionException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static registration.data.Animal.formNewAnimal;

public class AnimalTest {

    @Test
    public void formNewAnimalTest() {
        Animal expectedAnimal = new Animal();
        Map<String, String> properties = new HashMap<>();
        properties.put("ВЕС", "ЛЕГКОЕ");
        properties.put("РОСТ", "МАЛЕНЬКОЕ");
        properties.put("ТИП", "ВСЕЯДНОЕ");
        expectedAnimal.setProperties(properties);

        Animal actualAnimal = formNewAnimal("ЛЕГКОЕ, МАЛЕНЬКОЕ, ВСЕЯДНОЕ", formSigns());
        Assert.assertEquals(expectedAnimal, actualAnimal);
    }

    @Test
    public void formNewAnimalTestTwo() {
        Animal expectedAnimal = new Animal();
        Map<String, String> properties = new HashMap<>();
        properties.put("ВЕС", "ЛЕГКОЕ");
        properties.put("РОСТ", "МАЛЕНЬКОЕ");
        properties.put("ТИП", "ВСЕЯДНОЕ");
        expectedAnimal.setProperties(properties);

        Animal actualAnimal = formNewAnimal("МАЛЕНЬКОЕ, ЛЕГКОЕ, ВСЕЯДНОЕ", formSigns());
        Assert.assertEquals(expectedAnimal, actualAnimal);
    }

    @Test
    public void formNewAnimalTestThree() {
        Animal expectedAnimal = new Animal();
        Map<String, String> properties = new HashMap<>();
        properties.put("ВЕС", "ЛЕГКОЕ");
        properties.put("РОСТ", "МАЛЕНЬКОЕ");
        properties.put("ТИП", "ВСЕЯДНОЕ");
        expectedAnimal.setProperties(properties);

        Animal actualAnimal = formNewAnimal("ЛЕГКОЕ, ВСЕЯДНОЕ, МАЛЕНЬКОЕ", formSigns());
        Assert.assertEquals(expectedAnimal, actualAnimal);
    }

    @Test(expected = ProhibitedActionException.class)
    public void formNewAnimalTestWithException() {
        formNewAnimal("ЛЕГКОЕ, КАКОЕ-ТО, ВСЕЯДНОЕ", formSigns());
    }

    private List<Sign> formSigns() {
        return List.of(
                new Sign("ВЕС", List.of("ЛЕГКОЕ", "СРЕДНЕЕ", "ТЯЖЕЛОЕ")),
                new Sign("РОСТ", List.of("МАЛЕНЬКОЕ", "НЕВЫСОКОЕ", "ВЫСОКОЕ")),
                new Sign("ТИП", List.of("ТРАВОЯДНОЕ", "ПЛОТОЯДНОЕ", "ВСЕЯДНОЕ")));
    }
}
