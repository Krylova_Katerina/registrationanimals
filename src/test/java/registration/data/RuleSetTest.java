package registration.data;

import org.junit.Assert;
import org.junit.Test;
import registration.exception.ProhibitedActionException;

import java.util.HashMap;
import java.util.Map;

import static registration.data.RuleSet.addRule;

public class RuleSetTest {

    @Test
    public void addEqualRule() {
        RuleSet ruleSet = new RuleSet();
        addRule(ruleSet, "ТИП = ТРАВОЯДНОЕ");
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("ТИП", "ТРАВОЯДНОЕ");
        Assert.assertEquals(expectedMap, ruleSet.getEqualsRules());
    }


    @Test
    public void addNotEqualRule() {
        RuleSet ruleSet = new RuleSet();
        addRule(ruleSet, "ТИП != ТРАВОЯДНОЕ");
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("ТИП", "ТРАВОЯДНОЕ");
        Assert.assertEquals(expectedMap, ruleSet.getNotEqualsRules());
    }


    @Test
    public void addEqualAndNotEqualRule() {
        RuleSet ruleSet = new RuleSet();
        addRule(ruleSet, "ТИП != ТРАВОЯДНОЕ");
        addRule(ruleSet, "ТИП = ТРАВОЯДНОЕ");
        Map<String, String> expectedMap = new HashMap<>();
        expectedMap.put("ТИП", "ТРАВОЯДНОЕ");
        Assert.assertEquals(expectedMap, ruleSet.getEqualsRules());
        Assert.assertEquals(expectedMap, ruleSet.getEqualsRules());
    }

    @Test(expected = ProhibitedActionException.class)
    public void addRuleWithException() {
        RuleSet ruleSet = new RuleSet();
        addRule(ruleSet, "ТИПТРАВОЯДНОЕ");

    }
}
