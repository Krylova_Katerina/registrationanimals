package registration.data;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static registration.data.Sign.formNewSign;

public class SignTest {

    @Test
    public void formNewSignTest() {
        String signDescription = "ВЕС -> ЛЕГКОЕ, СРЕДНЕЕ, ТЯЖЕЛОЕ";
        Sign expectedSign = new Sign("ВЕС", List.of("ЛЕГКОЕ", "СРЕДНЕЕ", "ТЯЖЕЛОЕ"));
        Assert.assertEquals(expectedSign, formNewSign(signDescription));

    }
}
