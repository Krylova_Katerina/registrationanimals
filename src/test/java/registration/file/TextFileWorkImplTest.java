package registration.file;

import org.junit.Assert;
import org.junit.Test;
import registration.data.RuleSet;
import registration.data.Sign;

import java.util.List;

import static registration.data.RuleSet.addRule;

public class TextFileWorkImplTest {
    @Test
    public void readAnimalsDescriptionsFromFileTest() {
        FileWork textFileWork = new TextFileWorkImpl("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");

        List<String> resultDescription = textFileWork.readAnimalsDescriptionsFromFile();
        List<String> expectedDescription = List.of("ЛЕГКОЕ, МАЛЕНЬКОЕ, ВСЕЯДНОЕ", "ТЯЖЕЛОЕ, МАЛЕНЬКОЕ, ТРАВОЯДНОЕ",
                "НЕВЫСОКОЕ, ТРАВОЯДНОЕ, СРЕДНЕЕ", "ТЯЖЕЛОЕ, НЕВЫСОКОЕ, ТРАВОЯДНОЕ", "ТЯЖЕЛОЕ, ВЫСОКОЕ, ВСЕЯДНОЕ",
                "ПЛОТОЯДНОЕ, ЛЕГКОЕ, МАЛЕНЬКОЕ", "ЛЕГКОЕ, МАЛЕНЬКОЕ, ПЛОТОЯДНОЕ", "ЛЕГКОЕ, НЕВЫСОКОЕ, ТРАВОЯДНОЕ",
                "ЛЕГКОЕ, ВЫСОКОЕ, ВСЕЯДНОЕ", "СРЕДНЕЕ, МАЛЕНЬКОЕ, ВСЕЯДНОЕ", "СРЕДНЕЕ, НЕВЫСОКОЕ, ПЛОТОЯДНОЕ",
                "СРЕДНЕЕ, ВЫСОКОЕ, ТРАВОЯДНОЕ", "ТЯЖЕЛОЕ, МАЛЕНЬКОЕ, ТРАВОЯДНОЕ", "ТЯЖЕЛОЕ, НЕВЫСОКОЕ, ПЛОТОЯДНОЕ",
                "ТЯЖЕЛОЕ, ВЫСОКОЕ, ВСЕЯДНОЕ", "ЛЕГКОЕ, ПЛОТОЯДНОЕ, МАЛЕНЬКОЕ", "СРЕДНЕЕ, ВЫСОКОЕ, ТРАВОЯДНОЕ",
                "ТЯЖЕЛОЕ, МАЛЕНЬКОЕ, ВСЕЯДНОЕ");
        Assert.assertEquals(expectedDescription, resultDescription);
    }

    @Test
    public void readSignsFromFileTest() {
        FileWork textFileWork = new TextFileWorkImpl("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");

        List<Sign> resultSigns = textFileWork.readSignsFromFile();
        List<Sign> expectedSigns = List.of(
                new Sign("ВЕС", List.of("ЛЕГКОЕ", "СРЕДНЕЕ", "ТЯЖЕЛОЕ")),
                new Sign("РОСТ", List.of("МАЛЕНЬКОЕ", "НЕВЫСОКОЕ", "ВЫСОКОЕ")),
                new Sign("ТИП", List.of("ТРАВОЯДНОЕ", "ПЛОТОЯДНОЕ", "ВСЕЯДНОЕ")));
        Assert.assertEquals(expectedSigns, resultSigns);
    }

    @Test
    public void readRulesFromFileTest() {
        FileWork textFileWork = new TextFileWorkImpl("./src/test/resources/AnimalFile.txt",
                "./src/test/resources/RuleFile.txt",
                "./src/test/resources/SignFile.txt");

        RuleSet resultRuleSet = textFileWork.readRulesFromFile();
        RuleSet expectedRuleset = new RuleSet();
        addRule(expectedRuleset, "ТИП = всеядное");
        addRule(expectedRuleset, "РАЗМЕР != высокое");

        Assert.assertEquals(expectedRuleset, resultRuleSet);
    }
}
