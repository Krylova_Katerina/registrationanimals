package registration.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import registration.exception.ProhibitedActionException;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode
public class Animal {
    private Map<String, String> properties = new HashMap<>();

    public void setProperties(List<String> descriptions, List<Sign> signs) {
        descriptions = descriptions.stream().map(String::toUpperCase).collect(Collectors.toList());
        descriptions.forEach(description -> {
            signs.stream().filter(sign -> sign.getTypes().contains(description)).findFirst()
                    .ifPresentOrElse(sign -> properties.put(sign.getName(), description),
                            () -> {
                                throw new ProhibitedActionException("No such sign");
                            });
        });
    }

    public static Animal formNewAnimal(String animalDescription, List<Sign> signs) {
        String[] description = animalDescription.split(", ");
        Animal animal = new Animal();
        animal.setProperties(Arrays.asList(description), signs);
        return animal;
    }

}
