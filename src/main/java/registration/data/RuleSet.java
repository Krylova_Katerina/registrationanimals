package registration.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import registration.exception.ProhibitedActionException;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@EqualsAndHashCode
public class RuleSet {
    private Map<String, String> equalsRules = new HashMap<>();
    private Map<String, String> notEqualsRules = new HashMap<>();

    public static void addRule(RuleSet ruleSet, String rule) {
        String[] notEquals = rule.split(" != ");
        if (notEquals.length == 2) {
            ruleSet.notEqualsRules.put(notEquals[0].toUpperCase(), notEquals[1].toUpperCase());
        } else {
            String[] equals = rule.split(" = ");
            if (equals.length == 2) {
                ruleSet.equalsRules.put(equals[0].toUpperCase(), equals[1].toUpperCase());
            } else {
                throw new ProhibitedActionException("no rules");
            }
        }
    }
}
