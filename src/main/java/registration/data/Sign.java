package registration.data;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@EqualsAndHashCode
public class Sign {
    private String name;
    private List<String> types;

    public Sign(String name, List<String> types) {
        this.name = name;
        this.types = types;
    }

    public Sign() {
    }

    public static Sign formNewSign(String signDescription) {
        Sign sign = new Sign();
        String[] descriptionAndName = signDescription.split(" -> ");
        sign.setName(descriptionAndName[0].toUpperCase());
        sign.setTypes(Arrays.stream(descriptionAndName[1].split(", "))
                .map(String::toUpperCase).collect(Collectors.toList()));
        return sign;
    }

}
