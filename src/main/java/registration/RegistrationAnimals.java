package registration;

import registration.data.Animal;
import registration.data.RuleSet;
import registration.data.Sign;
import registration.file.FileWork;

import java.util.*;

import static registration.data.Animal.formNewAnimal;

public class RegistrationAnimals {
    private final FileWork fileWork;

    public RegistrationAnimals(FileWork fileWork) {
        this.fileWork = fileWork;
    }

    public boolean validateAnimalByRules(Animal animal, Map<String, String> rules, boolean equalOrNot) {
        boolean decision = true;
        for (String key : rules.keySet()) {
            String value = rules.get(key);
            List<String> orExpression = Arrays.asList(value.toUpperCase().split(" ИЛИ "));
            String propertyForCheck = animal.getProperties().get(key);
            decision = decision && orExpression.stream()
                    .anyMatch(or -> or.equals(propertyForCheck) == equalOrNot);
        }
        return decision;
    }

    public Long registrationAnimalsByRules() {
        RuleSet ruleSet = fileWork.readRulesFromFile();
        List<Sign> sign = fileWork.readSignsFromFile();
        List<String> animalDescriptions = fileWork.readAnimalsDescriptionsFromFile();

        Long countByRule = animalDescriptions.stream()
                .map(animalDescription -> formNewAnimal(animalDescription, sign))
                .filter(animal -> validateAnimalByRules(animal, ruleSet.getEqualsRules(), true)
                        && validateAnimalByRules(animal, ruleSet.getNotEqualsRules(), false))
                .count();

        System.out.println("There are " + countByRule + " such animals");
        return countByRule;
    }

}
