package registration.file;


import registration.data.RuleSet;
import registration.data.Sign;

import java.util.List;

public interface FileWork {

    List<String> readAnimalsDescriptionsFromFile();

    List<Sign> readSignsFromFile();

    RuleSet readRulesFromFile();
}
