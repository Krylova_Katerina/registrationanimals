package registration.file;

import registration.data.RuleSet;
import registration.data.Sign;
import registration.exception.ProhibitedActionException;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static registration.data.RuleSet.addRule;
import static registration.data.Sign.formNewSign;

public class TextFileWorkImpl implements FileWork {
    private final String pathDescriptionAnimals;
    private final String pathRules;
    private final String pathSigns;


    public TextFileWorkImpl(final String pathDescriptionAnimals,
                            final String pathRules,
                            final String pathSigns) {
        this.pathDescriptionAnimals = pathDescriptionAnimals;
        this.pathRules = pathRules;
        this.pathSigns = pathSigns;
    }

    @Override
    public List<String> readAnimalsDescriptionsFromFile() {
        List<String> resultAnimals = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(pathDescriptionAnimals))) {
            String animalDescription;
            while ((animalDescription = br.readLine()) != null) {
                if (!animalDescription.isBlank()) {
                    resultAnimals.add(animalDescription);
                }
            }
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error work with file", ex);
        }
        return resultAnimals;
    }

    @Override
    public List<Sign> readSignsFromFile() {
        List<Sign> resultSigns = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(pathSigns))) {
            String signDescription;
            while ((signDescription = br.readLine()) != null) {
                if (!signDescription.isBlank()) {
                    resultSigns.add(formNewSign(signDescription));
                }
            }
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error work with file", ex);
        }
        return resultSigns;
    }

    @Override
    public RuleSet readRulesFromFile() {
        RuleSet resultRule = new RuleSet();
        try (BufferedReader br = new BufferedReader(new FileReader(pathRules))) {
            String ruleDescription;
            while ((ruleDescription = br.readLine()) != null) {
                if (!ruleDescription.isBlank()) {
                    addRule(resultRule, ruleDescription);
                }
            }
        } catch (IOException ex) {
            throw new ProhibitedActionException("Error work with file", ex);
        }
        return resultRule;
    }


}
