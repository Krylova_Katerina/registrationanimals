package registration;

import registration.exception.ProhibitedActionException;
import registration.file.TextFileWorkImpl;

public class RegistrationAnimalsApplication {

    public static void main(final String[] args) {

        System.out.println("Animal registration");
        if (args.length != 3){
            throw  new ProhibitedActionException("not enough files");
        }

        TextFileWorkImpl textFileWork = new TextFileWorkImpl(args[0], args[1], args[2]);
        RegistrationAnimals registrationAnimals = new RegistrationAnimals(textFileWork);
        registrationAnimals.registrationAnimalsByRules();
    }

}
