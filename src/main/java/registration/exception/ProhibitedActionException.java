package registration.exception;

public class ProhibitedActionException extends RuntimeException {
    public ProhibitedActionException(final String message) {
        super(message);
    }

    public ProhibitedActionException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
